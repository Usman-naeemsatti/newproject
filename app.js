const express = require('express')
const app = express()

app.get('/', (req, res) => {
    res.send('This is my node application for cicd demo')
})

var listener = app.listen(8080, function(){
    console.log('Listening on port ' + listener.address().port); //Listening on port 8888
});
